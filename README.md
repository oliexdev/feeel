Feeel
=====

Feeel is an open-source app for doing simple at-home exercises.

This is a rewrite of the [original app](https://gitlab.com/enjoyingfoss/feeel-legacy) in Flutter, to make development easier and allow for an iOS version. Still free as in freedom.

Contribute
====
ANYONE can contribute, no special skills required. Message me on the [Feeel channel on Matrix](https://matrix.to/#/!jFShhgWHRXehKXrToU:matrix.org?via=matrix.org) if you'd like to help.

You can help by donating photos of exercises, [turning photos into low-poly illustrations](https://gitlab.com/enjoyingfoss/feeel/-/wikis/Processing-photos) (which anyone can do using an application made for the purpose), participating in user testing, [translating](https://www.transifex.com/enjoying-foss/feeel/), spreading the message, programming, or doing anything else that would help the project. :)

[More about contributing](https://gitlab.com/enjoyingfoss/feeel/-/wikis/home)

Donate
====
If you'd prefer to donate instead, you can donate through **[Liberapay](https://liberapay.com/Feeel/)**.

Install
====
[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get it on F-Droid"
      height="80">](https://f-droid.org/packages/com.enjoyingfoss.feeel/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
      alt="Get it on Google Play"
      height="80">](https://play.google.com/store/apps/details?id=com.enjoyingfoss.feeel)

F-droid still has the old version up. Waiting on this [merge request](https://gitlab.com/fdroid/fdroiddata/-/merge_requests/6540) to be accepted.
